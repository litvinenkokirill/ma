import _ from 'lodash';
import { act } from 'react-dom/test-utils';

const API_GET_AUTH_REQUEST = 'API_GET_AUTH_REQUEST';
const API_GET_AUTH_SUCCESS = 'API_GET_AUTH_SUCCESS';
const API_AUTH_FAILURE = 'API_GET_AUTH_FAILURE';

const API_GET_TEST_REQUEST = 'API_GET_TEST_REQUEST';
const API_GET_TEST_SUCCESS = 'API_GET_TEST_SUCCESS';
const API_GET_TEST_FAILURE = 'API_GET_TEST_FAILURE';

const API_POST_TEST_REQUEST = 'API_POST_TEST_REQUEST';
const API_POST_TEST_SUCCESS = 'API_POST_TEST_SUCCESS';
const API_POST_TEST_FAILURE = 'API_POST_TEST_FAILURE';

const SAVE_AN_ANSWER = 'SAVE_AN_ANSWER';

export const getAuthRequest = payload => ({
  type: API_GET_AUTH_REQUEST,
  payload,
});

export const getAuthSuccess = payload => ({
  type: API_GET_AUTH_SUCCESS,
  payload,
});

export const getAuthFailure = payload => ({
  type: API_AUTH_FAILURE,
  payload,
});

export const getTestRequest = () => ({
  type: API_GET_TEST_REQUEST,
});

export const getTestSuccess = payload => ({
  type: API_GET_TEST_SUCCESS,
  payload,
});

export const getTestFailure = payload => ({
  type: API_GET_TEST_FAILURE,
  payload,
});

export const postTestRequest = () => ({
  type: API_POST_TEST_REQUEST,
});

export const postTestSuccess = payload => ({
  type: API_POST_TEST_SUCCESS,
  payload,
});

export const postTestFailure = payload => ({
  type: API_POST_TEST_FAILURE,
  payload,
});

export const saveAnAnswer = payload => ({
  type: SAVE_AN_ANSWER,
  payload,
});

const initialState = {
  authResponse: {},
  authFetching: false,
  authFetched: false,
  authError: '',
  isLoggedIn: false,
  token: '',

  testQuestions: [],
  testFetching: false,
  testFetched: false,
  testError: '',

  sendingTest: false,
  sentTest: false,
  sendTestError: '',

  testAnswers: [],
};

export const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case API_AUTH_FAILURE:
      return {
        ...state,
        authFetching: false,
        authError: action.payload,
        authFetched: false,
      };
    case API_GET_AUTH_SUCCESS:
      return {
        ...state,
        authResponse: action.payload,
        authFetching: false,
        authFetched: true,
        isLoggedIn: true,
      };
    case API_GET_AUTH_REQUEST:
      return {
        ...state,
        token: action.payload,
        authFetching: true,
      };

    case API_GET_TEST_FAILURE:
      return {
        ...state,
        testFetching: false,
        testError: action.payload,
        testFetched: false,
      };
    case API_GET_TEST_SUCCESS:
      return {
        ...state,
        testQuestions: action.payload,
        testFetching: false,
        testFetched: true,
      };
    case API_GET_TEST_REQUEST:
      return {
        ...state,
        testFetching: true,
      };

    case API_POST_TEST_FAILURE:
      return {
        ...state,
        sendingTest: false,
        sentTest: false,
        sendTestError: action.payload,
      };
    case API_POST_TEST_SUCCESS:
      return {
        ...state,
        sendingTest: false,
        sentTest: true,
      };
    case API_POST_TEST_REQUEST:
      return {
        ...state,
        sendingTest: true,
      };

    case SAVE_AN_ANSWER:
      return {
        ...state,
        testAnswers: [...state.testAnswers, ...action.payload],
      };
    default:
      return state;
  }
};
