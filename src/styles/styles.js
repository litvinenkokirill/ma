import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({});

export const commonStyles = {
    loginPage: {
        display: 'flex',
        height: '100vh',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    testPage: {
        padding: 10,
    },
    titleWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    abRow: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        margin: '10px 0 20px',
    },
};