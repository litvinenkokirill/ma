import { takeLatest, call, put, select } from 'redux-saga/effects';
import axios from 'axios';
import { getAuthRequest, getAuthSuccess, getAuthFailure, mainReducer } from '../reducers/mainReducer';
import { API_LINK } from '../../constants';

function* authAxios() {
    const state = yield select(fullState => fullState);
    const response = yield call(() =>
        axios({
        method: 'get',
        url: `${API_LINK}/api/v1/me`,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            Authorization: `Bearer ${state.mainReducer.token}`,
        },
        }),
    );
    return response;
}

function* workerAuthSaga() {
  try {
    const response = yield call(authAxios);
    console.log(response.data);
    yield put(getAuthSuccess(response.data));
  } catch (error) {
    yield put(getAuthFailure(error));
  }
}

export function* watcherAuthSaga() {
  yield takeLatest(getAuthRequest().type, workerAuthSaga);
}
