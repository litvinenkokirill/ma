import { takeLatest, call, put, select } from 'redux-saga/effects';
import axios from 'axios';
import { getTestRequest, getTestSuccess, getTestFailure } from '../reducers/mainReducer';
import { API_LINK } from '../../constants';

function* getTestAxios() {
    // const state = yield select(fullState => fullState);
    // temp
    const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNTRjNmNhMzQ1YjBmMTk1Y2U1MTYzMDY5MjNiNjk0MTBjYmQ1NWJhNTJhZjQ5Y2ZmZTMxNzIzM2Y0YTE0ZjRmNWY4ZmM5ZDc4YjhlNGM3YmYiLCJpYXQiOjE1OTIwMzk2MzgsIm5iZiI6MTU5MjAzOTYzOCwiZXhwIjoxNjIzNTc1NjM4LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.ghUa6J6zBzxlxsclhGmNcHJbir7yFA6W3-XEcN2rud694hQ984OkFw9nwmntIGzGcoUqFQTsguTicoAC2ZdsFjJCuf2rdaZkbkBy4XwHrvx77VJ9nSiNW2jBSUBWADNFVlfTrhlEZ_m6gxt9YkpI9cFVEbmC55n4tn1D0Ck5kyPshxiltXHiVkMxglFl-BmMyy6Bebwb7AyifjNg1WkrZ2WGfGo3CZUFnWb7F7fuUObNohfV603mjEMJnAV672aUnT60QOkt7XllVi3R_wiI6TX-U72PPbL6zHTHGHpmXaeZ2zLh5w1VNxCCiJFvkR026KcDfoDSHMt5e7c6HYTMp6jskY4MQLPo1vgPzRYjM_23HneIeL4dQ-cocgRbKYubaT6IRa0FYP53bgHFCe-mczKIaCbbl-GbbDwBx_uogYBE-O-27TWZzb6eqau6LVmN7JelBEgR-OdFTPcH7czhTH1pCCNqmPXP6ZFPtxdSll6l6k9OJc_qBAn6xN9IIJWpRnYBM5UX9B02Fvx91jwnYuA2eRF6gLR97DiaUC0DNRjEDJPAUSyjybmqIJYqrrO1N3QziCavu_ZS7ajfNffyerc-LzDnmZjB7iInVbNdpyxB96Dw9a-Y5Ol15foEp_7NZPiGOat63_ec9MCUHMgLRvZu-XyuneoeYLAokxRs_eo';
    const response = yield call(() =>
        axios({
        method: 'get',
        url: `${API_LINK}/api/v1/quiz/1`,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            Authorization: `Bearer ${token}`,
        },
        }),
    );
    return response;
}

function* workerGetTestSaga() {
  try {
    const response = yield call(getTestAxios);
    yield put(getTestSuccess(response.data));
  } catch (error) {
    yield put(getTestFailure(error));
  }
}

export function* watcherGetTestSaga() {
  yield takeLatest(getTestRequest().type, workerGetTestSaga);
}
