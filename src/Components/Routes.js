import React from 'react';
import { Route, Switch } from 'react-router';

import Login from '../Screens/Login';
import Test from '../Screens/Test';

const Routes = () => (
  <Switch>
    <Route path="/login" exact component={Login} />
    <Route path="/" exact component={Test} />
  </Switch>
);

export default Routes;
