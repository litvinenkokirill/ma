import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withHandlers, lifecycle } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import { Button, Typography } from '@material-ui/core';

import { commonStyles } from '../styles/styles';

import {
    getTestRequest,
    saveAnAnswer,
    postTestRequest,
} from '../redux/reducers/mainReducer';

const Test = ({
    classes,
    mainState,
    handleSaveAnAnswer,
    handleSubmitTest,
}) => {
    return (
        <div className={classes.testPage}>
            <div className={classes.titleWrapper}>
                <Typography variant="h5" gutterBottom>{mainState.testQuestions.name}</Typography>
            </div>
            {mainState.testFetched && mainState.testQuestions.questions.map(q => {
                return !(mainState.testAnswers.some(a => a.question_id === q.id)) && (
                    <div key={q.id}>
                        <Typography variant="body1" gutterBottom>{q.question}</Typography>
                        <div className={classes.abRow}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => handleSaveAnAnswer(q.id, 'a')
                            }>yes</Button>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => handleSaveAnAnswer(q.id, 'b')}
                            >no</Button>
                        </div>
                    </div>
                )
            })}
            <Button
                variant="contained"
                color="primary"
                onClick={() => handleSubmitTest()}
                disabled={mainState.testAnswers.length !== 20}
            >submit test</Button>
        </div>
)};

const mapStateToProps = state => ({
    mainState: state.mainReducer,
});

const mapDispatchToProps = dispatch => ({
    getTestRequest: () => dispatch(getTestRequest()),
    saveAnAnswer: answer => dispatch(saveAnAnswer(answer)),
    postTestRequest: () => dispatch(postTestRequest()),
});


const styles = theme => ({
  ...commonStyles,
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withHandlers({
        handleSaveAnAnswer: props => (qId, option) => {
            props.saveAnAnswer([{'question_id': qId, 'answer': option}])
        },
        handleSubmitTest: props => () => {
            props.postTestRequest();
        }, 
    }),
    withStyles(styles),
    lifecycle({
        componentWillMount() {
            const {
                getTestRequest,
            } = this.props;
            getTestRequest();
        },
      }),
)(Test);

Test.propTypes = {
  classes: PropTypes.instanceOf(Object),
};