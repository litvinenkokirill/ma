import { all } from 'redux-saga/effects';
import { watcherAuthSaga } from './auth';
import { watcherGetTestSaga } from './getTest';
import { watcherPostTestSaga } from './postTest';

export default function* rootSaga() {
  yield all([
    watcherAuthSaga(),
    watcherGetTestSaga(),
    watcherPostTestSaga(),
  ]);
}
