import React from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';

import Routes from './Components/Routes';

import { theme, commonStyles } from './styles/styles';

const App = ({ classes }) => (
  <MuiThemeProvider theme={theme}>
      <div className={classes.root}>
        <Routes />
      </div>
  </MuiThemeProvider>
);

App.propTypes = {
  classes: PropTypes.instanceOf(Object),
};

const styles = () => ({
  ...commonStyles,
});

export default compose(
  withRouter,
  withStyles(styles),
)(App);
