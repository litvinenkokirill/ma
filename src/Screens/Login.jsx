import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withHandlers } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

import { commonStyles } from '../styles/styles';

import {
    getAuthRequest,
    getAuthSuccess,
    getAuthFailure,
} from '../redux/reducers/mainReducer';

const Login = ({
    classes,
    mainState,
    getToken,
    authVerification,
}) => {
    const tokenText = useRef(null);
    return (
        <div className={classes.loginPage}>
            <Button variant="contained" onClick={() => getToken()}>login with a facebook</Button>
            <textarea name="token" id="token" cols="30" rows="10" ref={tokenText}></textarea>
            <Button variant="contained" onClick={() => authVerification(tokenText.current.value)}>verify token</Button>
            <NavLink to="/test">Take a test</NavLink>
        </div>
)};

const mapStateToProps = state => ({
    mainState: state.mainReducer,
});

const mapDispatchToProps = dispatch => ({
    getAuthRequest: token => dispatch(getAuthRequest(token)),
});


const styles = theme => ({
  ...commonStyles,
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withHandlers({
        getToken: () => {
            window.open("https://711bfbf1aff9.ngrok.io/matching_app/matching_app_backend/public/social/auth/facebook");
        },
        authVerification: props => token => {
            props.getAuthRequest(token);
        },
    }),
    withStyles(styles),
)(Login);

Login.propTypes = {
  classes: PropTypes.instanceOf(Object),
};